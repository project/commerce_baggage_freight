<?php

/**
 * @file
 * Contains Drupal\commerce_baggage_freight\Response\ResponseInterface;
 */

namespace Drupal\commerce_baggage_freight\Response;

interface ResponseInterface {
  public function hasErrors();
  public function getErrors();
  public function getResponseComponents();
  public function getResponseRaw();
}
