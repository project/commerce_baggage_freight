<?php

/**
 * @file
 * Contains BaggageFreight\api\BaggageFreightResponse.
 */

namespace Drupal\commerce_baggage_freight\Response;

/**
 * A class which wraps any Baggage Freight API response to provide useful info.
 */
class Response implements ResponseInterface {

  /**
   * The HTTP response string from the endpoint.
   *
   * @var \stdClass
   */
  protected $httpResponse;

  /**
   * A list of errors from the response.
   *
   * @var array
   */
  protected $errors = [];

  /**
   * Construct a new response object.
   *
   * @param $response
   *   The HTTP response from drupal_http_request().
   */
  function __construct(\stdClass $response) {
    if ($response->code === '200' && empty($response->data)) {
      throw new \InvalidArgumentException(t('HTTP Response was empty. Please check your configuration.'));
    }
    $this->httpResponse = $response;
  }

  /**
   * Does the request have errors, or was the API request valid?
   *
   * @return bool
   */
  public function hasErrors() {
    $has_errors = FALSE;
    $this->errors = [];

    if ($this->httpResponse->data == '0') {
      $has_errors = TRUE;
      $this->errors[] = 'The last call to BF failed, due to status code "0" being returned.';
    }

    if (isset($this->httpResponse->error)) {
      $has_errors = TRUE;
      $this->errors[] = $this->httpResponse->error;
    }
    return $has_errors;
  }

  /**
   * Get the errors returned from the response.
   *
   * @return array
   *   The list of errors.
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * Some calls split info up using colons. Return individual components.
   *
   * @return array
   *   A list of components returned by the API call.
   */
  public function getResponseComponents() {
    return explode(':', $this->httpResponse->data);
  }

  /**
   * Get the raw response.
   *
   * @return string
   *   The raw HTTP response.
   */
  public function getResponseRaw() {
    return $this->httpResponse->data;
  }

}
