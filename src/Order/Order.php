<?php

/**
 * @file
 * Contains \Drupal\commerce_baggage_freight\Order\Order
 */

namespace Drupal\commerce_baggage_freight\Order;

/**
 * Translate a commerce order into information useful for Baggage Freight.
 */
class Order extends OrderBase {

  /**
   * Get the fields required for an order that are specific to the order.
   *
   * Documented here: http://www.baggagefreight.com.au/api/MakeOrder.aspx
   *
   * @return array
   *   An array of fields and values for the API call.
   */
  public function getFields() {

    $physical_fields = $this->getPhysicalFields();
    $address_fields = $this->getAddressFields();
    $order_fields = $this->getOrderFields();

    return array_merge($order_fields, $address_fields, $physical_fields);
  }

  /**
   * Get the fields off an order to make a Baggage Freight order.
   *
   * @return array
   *   The fields for an order off the commerce order.
   */
  public function getOrderFields() {

    // The API requires us to send back all of the information we got from
    // their quote.
    $quote = $this->order->data['baggage_freight_quote'];
    $fields = array(
      'strInvoiceNumber' => $this->order->order_id,
      'strBookingAmount' => $quote['shipping_cost'] / 100,
      'strCarrier' => $quote['carrier'],
      'strService' => $quote['service'],
      'coverCost' => $quote['warranty'] / 100,
      'strTransitTime' => $quote['transit_time'],
    );

    // Send purchased product metadata to BF.
    $fields['strTotalBookingRate'] = 0;
    $fields['strUnitVal'] = '';
    $fields['strDescription'] = '';
    foreach ($this->getWrapper()->commerce_line_items->getIterator() as $line_item) {
      if ($line_item->type->value() == 'product' && commerce_physical_line_item_shippable($line_item->value())) {
        // Get the decimal cost of the procuts on this line item.
        $product_cost = commerce_price_component_total($line_item->commerce_total->value());
        $product_cost_decimal = commerce_currency_amount_to_decimal($product_cost['amount'], $product_cost['currency_code']);
        // Add the total amount of the line item to the fields.
        $fields['strTotalBookingRate'] += $product_cost_decimal;
        $product_sku = $line_item->commerce_product->sku->value();
        // For every product in the line item, add some information to the fields.
        foreach (range(1, $line_item->quantity->value()) as $quantity_item) {
          // Prices for each product.
          $fields['strUnitVal'] .= ',' . $product_cost_decimal;
          // Product SKUs.
          $fields['strDescription'] .= ',' . $product_sku;
        }
      }
    }

    return $this->trimTrailingCommas($fields);
  }

  /**
   * Get the physical fields required for an order.
   *
   * @return array
   *   The physical fields required to make an order.
   */
  public function getPhysicalFields() {
    $quote_fields = $this->getPhysicalFields();
    $order_fields = array();
    foreach ($quote_fields as $field_name => $value) {
      // For some reason the keys against the order and the quote are different.
      $order_fields['arr' . $field_name] = $value;
    }
    return $order_fields;
  }

  /**
   * Get the address fields required to make a booking.
   */
  public function getAddressFields() {
    $order = $this->getWrapper();
    $shipping_profile = $order->commerce_customer_shipping;
    $customer_address = $shipping_profile->commerce_customer_address->value();

    require_once DRUPAL_ROOT . '/includes/locale.inc';
    $countries = country_get_list();

    return array(
      'strDestContactName' => $customer_address['name_line'],
      'strDestEmail' => $order->mail->value(),
      'strDestPhNo' => '',
      'strDestCompany' => $customer_address['organisation_name'],
      'strDestAddress' => $customer_address['thoroughfare'],
      'strDestAddress1' => $customer_address['premise'],
      'strDestCity' => $customer_address['locality'],
      'strDestState' => $customer_address['administrative_area'],
      'strDestZip' => $customer_address['postal_code'],
      'strDestCountry' => $countries[$customer_address['country']],
    );
  }

}
