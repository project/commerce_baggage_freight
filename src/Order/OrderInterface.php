<?php

/**
 * @file
 * Contains \Drupal\commerce_baggage_freight\Order\Order
 */

namespace Drupal\commerce_baggage_freight\Order;

/**
 * Translate a commerce order into information useful for Baggage Freight.
 */
interface OrderInterface {

  /**
   * Get the fields that entity provides.
   *
   * @return array
   *   The fields for this entity.
   */
  public function getFields();

  /**
   * Get an Entity API object to make it easier to work with.
   *
   * @return \EntityMetadataWrapper
   */
  public function getWrapper();
}
