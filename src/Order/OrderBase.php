<?php

/**
 * @file
 * Contains \Drupal\commerce_baggage_freight\Order\Order
 */

namespace Drupal\commerce_baggage_freight\Order;

/**
 * Translate a commerce order into information useful for Baggage Freight.
 */
abstract class OrderBase implements OrderInterface {

  /**
   * The commerce order object.
   *
   * @var \stdClass
   */
  protected $order;

  /**
   * The wrapper object.
   *
   * @var \EntityMetadataWrapper
   */
  protected $wrapper;

  /**
   * @param $commerce_order
   *   Any valid commerce order.
   */
  function __construct($commerce_order) {
    $this->order = $commerce_order;
  }

  /**
   * Get the fields from the order which represent the physcial fields.
   *
   * @return array
   *   The physical fields required for a quote.
   */
  protected function getPhysicalFields() {
    $fields = array(
      'Weight' => '',
      'Width' => '',
      'Height' => '',
      'Length' => '',
      'Unit' => '',
    );

    $order_w = $this->getWrapper();
    foreach ($order_w->commerce_line_items->getIterator() as $line_item) {

      // Make sure the line item is shippable, ie of type "product".
      if (!commerce_physical_line_item_shippable($line_item->value())) {
        continue;
      }

      // Add the weight to the requested fields.
      $weight = commerce_physical_product_line_item_weight($line_item->value());
      $kg_weight = physical_weight_convert($weight, 'kg');
      // Add the dimensions from each line item to the fields as per the spec.
      $dimensions = commerce_physical_product_line_item_dimensions($line_item->value());
      // For the quotes, all sizes must be in cm.
      $dimensions = physical_dimensions_convert($dimensions, 'cm');

      // We need an entry for each product quantity on the line item.
      foreach (range(1, $line_item->quantity->value()) as $quantity_item) {
        $fields['Weight'] .= ',' . $kg_weight['weight'];
        foreach ($dimensions as $dimension => $value) {
          $fields[ucfirst($dimension)] .= ',' . $value;
        }
      }
    }

    return $this->trimTrailingCommas($fields);
  }

  /**
   * Get the shipping service used by the order.
   *
   * @return string
   *   The machine name of the shipping service used.
   */
  public function getShippingService() {
    $shipping_line_item_w = FALSE;
    foreach ($this->getWrapper()->commerce_line_items->getIterator() as $line_item) {
      if ($line_item->type->value() == 'shipping') {
        $shipping_line_item_w = $line_item;
        break;
      }
    }
    return $shipping_line_item_w->commerce_shipping_service->value();
  }

  /**
   * {@inheritdoc}
   */
  public function getWrapper() {
    if (!isset($this->wrapper)) {
      $this->wrapper = entity_metadata_wrapper('commerce_order', $this->order);
    }
    return $this->wrapper;
  }

  /**
   * Trim trailing commas off an array of fields.
   *
   * @param array $fields
   *   The initial fields.
   *
   * @return array
   *   The trimmed fields.
   */
  public function trimTrailingCommas(&$fields) {
    // Trim all fields for leading or trailing commas.
    foreach ($fields as $key => &$value) {
      $value = trim($value, ',');
    }
    return $fields;
  }

}
