<?php

/**
 * @file
 * Contains \Drupal\commerce_baggage_freight\Order\Quote
 */

namespace Drupal\commerce_baggage_freight\Order;

/**
 * A wrapper for the commerce order when it's still a quote.
 */
class Quote extends OrderBase {

  /**
   * Get the fields required for a quote.
   *
   * Documented here: http://www.baggagefreight.com.au/api/getQuote.aspx
   *
   * @return array
   *   The fields required for a quote.
   */
  public function getFields() {
    $physical_fields = $this->getPhysicalFields();
    $address_fields = $this->getAddressQuoteFields();
    return array_merge($physical_fields, $address_fields);
  }

  /**
   * Get the fields from the order which represent the address fields.
   *
   * @return array
   *   The address fields required for a quote.
   */
  protected function getAddressQuoteFields() {
    $shipping_profile = $this->getWrapper()->commerce_customer_shipping;
    $customer_address = $shipping_profile->commerce_customer_address->value();

    require_once DRUPAL_ROOT . '/includes/locale.inc';
    $countries = country_get_list();

    return array(
      'dCountry' => $countries[$customer_address['country']],
      'dState' => $customer_address['administrative_area'],
      'dCity' => strtoupper($customer_address['locality']),
      'dPin' => $customer_address['postal_code'],
    );
  }

}
