<?php
/**
 * @file
 * Contains BaggageFreight\api\BaggageFreightRequest.
 */

namespace Drupal\commerce_baggage_freight\Request;

use Drupal\commerce_baggage_freight\Response\Response;

/**
 * A request class for all Baggage Freight API calls.
 */
class Request implements RequestInterface {

  protected $endpoint;
  protected $fields = array();
  protected $apiPath;
  protected $apiUsername;
  protected $apiPassword;
  protected $debug;

  /**
   * Construct a new request object.
   */
  function __construct() {
    $this->apiPath = variable_get('baggage_freight_endpoint', '');
    $this->apiUsername = variable_get('baggage_freight_username', '');
    $this->apiPassword = variable_get('baggage_freight_password', '');
    $this->debug = variable_get('baggage_freight_debug', TRUE);
  }

  /**
   * Execute the request to Baggage Freight.
   *
   * @return \Drupal\commerce_baggage_freight\Response\ResponseInterface
   *   The response from the API.
   */
  public function execute() {
    $endpoint_url = $this->apiPath . '/' . $this->endpoint;
    $request_body = array(
      // Sometimes different keys are used for the credentials.
      'Email' => $this->apiUsername,
      'Password' => $this->apiPassword,
      'strEmail' => $this->apiUsername,
      'strPassword' => $this->apiPassword,
    );
    $request_body = array_merge($request_body, $this->fields);

    $response = drupal_http_request($endpoint_url, array(
      'method' => 'POST',
      'headers' => array(
        'Content-Type' => 'application/x-www-form-urlencoded',
      ),
      'data' => drupal_http_build_query($request_body),
    ));

    if ($this->debug) {
      watchdog('commerce_baggage_freight', 'A request has been made to baggage freight. Endpoint: @endpoint Request body: @body', array(
        '@endpoint' => $endpoint_url,
        '@body' => var_export($request_body, TRUE),
      ), WATCHDOG_INFO);
    }

    return new Response($response);
  }

  /**
   * Set the endpoint we will be hitting for the request.
   *
   * @param string $endpoint
   *   The endpoint appended to the URL.
   */
  public function setEndpoint($endpoint) {
    $this->endpoint = $endpoint;
  }

  /**
   * Set the fields which will be sent to the server (excluding username/pass).
   *
   * @param array $fields
   *   The fields for the request.
   */
  public function setFields($fields) {
    $this->fields = $fields;
  }

  /**
   * Get the fields for the request.
   *
   * @return array
   *   The fields for the request.
   */
  public function getFields() {
    return $this->fields;
  }

}
