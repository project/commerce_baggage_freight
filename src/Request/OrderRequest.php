<?php
/**
 * @file
 * Contains \Drupal\commerce_baggage_freight\Request\OrderRequest
 */


namespace Drupal\commerce_baggage_freight\Request;

use Drupal\commerce_baggage_freight\Order\Order;

/**
 * A request class for all Baggage Freight orders.
 */
class OrderRequest extends Request {

  function __construct(Order $quote) {
    parent::__construct();

    $this->setEndpoint('doBooking.aspx');

    $order_fields = $quote->getFields();
    $standard_fields = $this->getStandardOrderFields();

    $this->setFields(array_merge($order_fields, $standard_fields));
  }

  /**
   * Get the standard fields which are global to all API orders.
   */
  protected function getStandardOrderFields() {
    return array(
     'strCollectCompany' => variable_get('baggage_freight_collection_company', ''),
     'strCollContactName' => variable_get('baggage_freight_collection_company', ''),
     'strCollectAddress' => variable_get('baggage_freight_collection_address_line_1', ''),
     'strCollectAddress1' => variable_get('baggage_freight_collection_address_line_2', ''),
     'strCollectCity' => strtoupper(variable_get('baggage_freight_collection_address_city', '')),
     'strCollectState' => strtoupper(variable_get('baggage_freight_collection_address_state', '')),
     'strCollectZip' => variable_get('baggage_freight_collection_address_pin', ''),
     'strCollectCountry' => variable_get('baggage_freight_collection_address_country', ''),
     'strCollectEmail' => variable_get('baggage_freight_collection_email', ''),
     'strCollectPhNo' => variable_get('baggage_freight_collection_phone', ''),
     'strUrl' => variable_get('baggage_freight_store_url', ''),
     'strEmail' => variable_get('baggage_freight_store_email', ''),
    );
  }
}
