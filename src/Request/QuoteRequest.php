<?php
/**
 * @file
 * Contains \Drupal\commerce_baggage_freight\Request\QuoteRequest
 */

namespace Drupal\commerce_baggage_freight\Request;

use Drupal\commerce_baggage_freight\Order\Quote;

/**
 * A request class for baggage freight.
 */
class QuoteRequest extends Request {

  function __construct(Quote $quote) {
    parent::__construct();

    $this->setEndpoint('apiminrate.aspx');

    $order_fields = $quote->getFields();
    $standard_fields = $this->getStandardQuoteFields();

    $this->setFields(array_merge($order_fields, $standard_fields));
  }

  /**
   * The the fields which are standard to all quotes.
   */
  protected function getStandardQuoteFields() {
    return array(
      'cCountry' => variable_get('baggage_freight_collection_address_country', ''),
      'cCity' => strtoupper(variable_get('baggage_freight_collection_address_city', '')),
      'cState' => strtoupper(variable_get('baggage_freight_collection_address_state', '')),
      'cPin' => variable_get('baggage_freight_collection_address_pin', ''),
    );
  }

}
