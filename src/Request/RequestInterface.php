<?php

/**
 * @file
 * Contains \Drupal\commerce_baggage_freight\Request\RequestInterface;
 */

namespace Drupal\commerce_baggage_freight\Request;

interface RequestInterface {
  public function execute();
  public function setEndpoint($endpoint);
  public function setFields($fields);
}
