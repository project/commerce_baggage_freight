<?php

/**
 * @file
 * Handle the admin settings form.
 */

/**
 * The admin form to configure the API for use.
 */
function commerce_baggage_freight_admin_configure_form($form, &$form_state) {

  $form['api_information'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Authentication'),
    '#description' => t('The details used to connted and send information to your Baggage Freight store.'),
    'children' => array(
      'baggage_freight_username' => array(
        '#title' => t('Username'),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_username', ''),
      ),
      'baggage_freight_password' => array(
        '#title' => t('Password'),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_password', ''),
      ),
      'baggage_freight_endpoint' => array(
        '#type' => 'textfield',
        '#title' => 'API Endpoint',
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_endpoint', 'http://www.baggagefreight.com.au/api'),
      ),
    )
  );

  $form['collection_address'] = array(
    '#type' => 'fieldset',
    '#title' => t('Collection Information'),
    '#description' => t('These details will be sent to Baggage Freight during orders as the collection point for packages.'),
    'children' => array(
      'baggage_freight_collection_contact_person' => array(
        '#type' => 'textfield',
        '#title' => t('Contact Person'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_contact_person', ''),
      ),
      'baggage_freight_collection_phone' => array(
        '#type' => 'textfield',
        '#title' => t('Phone Number'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_phone', ''),
      ),
      'baggage_freight_collection_email' => array(
        '#type' => 'textfield',
        '#title' => t('Email Address'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_email', ''),
      ),
      'baggage_freight_collection_company' => array(
        '#type' => 'textfield',
        '#title' => t('Company Name'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_company', ''),
      ),
      'baggage_freight_collection_address_line_1' => array(
        '#type' => 'textfield',
        '#title' => t('Address (Line 1)'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_address_line_1', ''),
      ),
      'baggage_freight_collection_address_line_2' => array(
        '#type' => 'textfield',
        '#title' => t('Address (Line 2)'),
        '#default_value' => variable_get('baggage_freight_collection_address_line_2', ''),
      ),
      'baggage_freight_collection_address_city' => array(
        '#type' => 'textfield',
        '#title' => t('City'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_address_city', ''),
      ),
      'baggage_freight_collection_address_pin' => array(
        '#type' => 'textfield',
        '#title' => t('Postal Code'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_address_pin', ''),
      ),
      'baggage_freight_collection_address_state' => array(
        '#type' => 'textfield',
        '#title' => t('State'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_address_state', ''),
      ),
      'baggage_freight_collection_address_country' => array(
        '#type' => 'textfield',
        '#title' => t('Country'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_collection_address_country', ''),
      ),
    ),
  );

  $form['store_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Store Settings'),
    '#description' => t('The information about your store to send to Baggage Freight.'),
    'children' => array(
      'baggage_freight_store_url' => array(
        '#type' => 'textfield',
        '#title' => t('Store URL'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_store_url', ''),
      ),
      'baggage_freight_store_email' => array(
        '#type' => 'textfield',
        '#title' => t('Store Email Address'),
        '#required' => TRUE,
        '#default_value' => variable_get('baggage_freight_store_email', ''),
      ),
    ),
  );

  return system_settings_form($form);
}
