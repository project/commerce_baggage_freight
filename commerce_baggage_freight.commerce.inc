<?php

/**
 * @file
 * Drupal Commerce hooks.
 */

use Drupal\commerce_baggage_freight\Request\QuoteRequest;
use Drupal\commerce_baggage_freight\Request\OrderRequest;
use Drupal\commerce_baggage_freight\Order\Order;
use Drupal\commerce_baggage_freight\Order\Quote;

// Baggage Freight only supports AUD.
define('COMMERCE_BAGGAGE_FREIGHT_CURRENCY_CODE', 'AUD');

/**
 * Implements hook_commerce_shipping_method_info().
 */
function commerce_baggage_freight_commerce_shipping_method_info() {
  return array(
    'commerce_baggage_freight_method' => array(
      'title' => t('Commerce Baggage Freight'),
      'display_title' => t('Baggage Freight'),
      'description' => t('Integration with the Baggage Freight service.'),
      'active' => TRUE,
    ),
  );
}

/**
 * Implements hook_commerce_shipping_service_info().
 */
function commerce_baggage_freight_commerce_shipping_service_info() {
  return array(
    'commerce_baggage_freight_service' => array(
      'title' => t('Baggage Freight Cheapest Quote'),
      'shipping_method' => 'commerce_baggage_freight_method',
      'callbacks' => array(
        'rate' => 'commerce_baggage_freight_calculate_rate',
      ),
    ),
//    'commerce_baggage_freight_service_no_warranty' => array(
//      'title' => t('Baggage Freight Cheapest Quote (No Warranty)'),
//      'shipping_method' => 'commerce_baggage_freight_method',
//      'callbacks' => array(
//        'rate' => 'commerce_baggage_freight_calculate_rate',
//      ),
//    ),
  );
}

/**
 * Callback to calculate the shipping rate.
 */
function commerce_baggage_freight_calculate_rate($service, $commerce_order) {

  // Determine if the proper physical dimensions have been added to this order.
  if (!commerce_physical_order_shippable($commerce_order)) {
    return;
  }

  $quote = commerce_baggage_freight_get_quotes_from_order($commerce_order);
  // If we cannot get a quote, our item cannot be shipped via BF.
  if (!$quote) {
    return FALSE;
  }

  $price = commerce_price_field_data_auto_creation();
  $price['currency_code'] = COMMERCE_BAGGAGE_FREIGHT_CURRENCY_CODE;
  $price['amount'] = $quote['shipping_cost'] + $quote['warranty'];

  return $price;
}

/**
 * Implements hook_commerce_checkout_complete().
 */
function commerce_baggage_freight_commerce_checkout_complete($commerce_order) {
  // Only push the order into BF if they have chosen to ship with them.
  $order = new Order($commerce_order);
  if ($order->getShippingService() !== 'commerce_baggage_freight_service') {
    return;
  }
  // Log a message and then push the order into BF.
  watchdog('baggage_freight', 'A user completed an order with BF. Order ID: @order_id', array('@order_id' => $commerce_order->order_id));
  $api_order = new OrderRequest($order);
  $response = $api_order->execute();
  // Log if we get a failed status code.
  if ($response->hasErrors()) {
    watchdog('baggage_freight', implode(',', $response->getErrors()), array('@order_id' => $commerce_order->order_id), WATCHDOG_CRITICAL);
  }
}

/**
 * Implements hook_commerce_shipping_service_rate_options_alter().
 */
function commerce_baggage_freight_commerce_shipping_service_rate_options_alter(&$options, $order) {
  if (!isset($options['commerce_baggage_freight_service'])) {
    return;
  }
  // Purely for display, tell the user what they are actually getting.
  $quote = $order->data['baggage_freight_quote'];
  $options['commerce_baggage_freight_service'] = $quote['carrier'] . ' (' . $quote['service'] . '): ' . commerce_currency_format($quote['shipping_cost'] + $quote['warranty'], COMMERCE_BAGGAGE_FREIGHT_CURRENCY_CODE);
  $options['commerce_baggage_freight_service'] .= '<div class="description">';
  $options['commerce_baggage_freight_service'] .= t('Transit time: @days days. Includes @warranty transit warranty.', array('@days' => $quote['transit_time'], '@warranty' => commerce_currency_format($quote['warranty'], COMMERCE_BAGGAGE_FREIGHT_CURRENCY_CODE)));
  $options['commerce_baggage_freight_service'] .= '</div>';
}

/**
 * Get quotes for an order and cache them for the current page load and order.
 *
 * @param $commerce_order
 *   The commerce order to get quotes for.
 *
 * @return array
 *   An array of components from Baggage Freight.
 */
function commerce_baggage_freight_get_quotes_from_order($commerce_order) {

  $quote_cache = & drupal_static(__FUNCTION__);
  if (isset($quote_cache[$commerce_order->order_id])) {
    return $quote_cache[$commerce_order->order_id];
  }

  // Query for the Baggage Freight quote.
  $quote = new Quote($commerce_order);
  $quote = new QuoteRequest($quote);
  $response = $quote->execute();

  // Normalise the components correctly.
  $components = $response->getResponseComponents();
  if (count($components) < 5) {
    return FALSE;
  }

  // @TODO, Move all of this into a QuoteResponse object.
  // The API needs work... It returns colon delimited fields, EXCEPT
  // colons appear in their service names! So, sometimes you have 5 elements
  // and sometimes you have 6.
  $total_components = count($components);
  $components = array(
    'shipping_cost' => commerce_currency_decimal_to_amount($components[0], COMMERCE_BAGGAGE_FREIGHT_CURRENCY_CODE),
    'carrier' => $components[1],
    'service' => $components[2],
    'transit_time' => $components[$total_components - 2],
    'warranty' => commerce_currency_decimal_to_amount($components[$total_components - 1], COMMERCE_BAGGAGE_FREIGHT_CURRENCY_CODE),
  );
  // Save the quote for use later on.
  $commerce_order->data['baggage_freight_quote'] = $components;
  commerce_order_save($commerce_order);

  $quote_cache[$commerce_order->order_id] = $components;
  return $components;
}
